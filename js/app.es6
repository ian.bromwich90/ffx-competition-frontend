/* Final Fantasy PreOrder Project
================================
Small front-end for logging gamertag
pre orders.
*/

class FfxPreorder {
    /**
    Attempt to get gamertag
    and trigger loaders
    */
    constructor() {
        
        /* Gamerscore Properties
        ------------------------- */

        /** User object storing gamertag, name and profile picture */
        this.user = {
            'name': null,
            'gamertag': null,
            'gamerpic': null
        };

        /** Store user details not tied to gamertag */
        this.userEntered = false;

        /** STX instance */
        this.stx = new Stx2();

        /** Set Locale */
        this.locale = this.stx.getLocale();

        /** Default signed in status to false. This is updated after successful logins via STX */
        this.signedInStatus = false;

        /** Return URL that the user will be redirected to once logging in via Xbox.com's account section */
        this.returnUrl = this.getReturnUrl();

        /** RESTful API endpoint for testing gamertags */
        this.endpoint = 'https://ffx-preorder.azurewebsites.net/api/';

        /** Final fantasy microsoft store URL */
        this.storeUrl = "https://www.microsoft.com/" + this.locale + "/store/p/final-fantasy-xv/bzx2l7ns942k";
        
        /** Competition end dates for selected locales */
        this.expiryDate = {
            'en-sg': new Date('12-1-2016'),
            'ko-kr': new Date('12-1-2016'),
            'zh-tw': new Date('12-1-2016'),
            'en-hk': new Date('12-1-2016'),
			'zh-hk': new Date('12-1-2016')
        };

        /** `modalSettings` determine modal behaviour on page load */
        this.modalSettings = this.getParameterByName('modal');

        // Init app
        this.init();
    }



    /* Init
    ============================
    Responsible for initializing the
    entire app, including content on
    page load */
    init() {

        // Register event listeners
        this.registerEventListeners();


        // Check login status
        // and load content
        this.loadContent();
    }



    /* Find User
    ============================
    Checks current Xbox.com
    logged in user then loads
    relevant content
    */
    loadContent() {

        // Get user profile
        // via STX
        this.stx.getLogin(e => {
            if (!this.signedInStatus) {
                this.signedInStatus = true;

                if (e.success === true) {

                    // User is logged in so
                    // set user properties
                    this.user.name = e.user.firstName || null;
                    this.user.gamertag = e.user.gamerTag || null;
                    this.user.gamerpic = e.user.gamerPic || null;


                    // Catch STX anomalies
                    if (this.user.gamertag === null) {

                        // Falls back to logged
                        // out home page
                        this.buildView();


                    // Check whether a user has
                    // already clicked pre-order
                    } else {

                        // Check the user and
                        // build the view
                        this.checkUser(this.user.gamertag);
                    }

                } else {

                    // Fall back to logged out
                    // landing page if the user
                    // is not logged in
                    this.buildView();
                }
            }
        });
    }



    /* Check User
    ============================
    Check whether a user has
    already clicked pre-order
    */
    checkUser(gamertag) {
        let userCheck = $.getJSON(this.endpoint + 'checkGamertag?gamertag=' + gamertag);

        $.when(userCheck)

            // Promise success
            .done(e => {
                
                // If gamertag exists, flag
                // the boolean as true
                if (e.status === 'found') {
                    this.user.date = e.date;
                    this.buildView('existing');
                } else {
                    this.buildView('new');
                }
            })
            .fail(e => {
                this.buildView();
            });
    }


    /* Get location path
    ============================
    Builds a string of the current
    URL minus any parameters
    */
    getLocationPath() {
        return location.protocol + '//' + location.host + location.pathname;
    }


    /* Build View
    ============================
    Builds relevant page content
    based on current logged in
    (or logged out) users
    */
    buildView(type = 'loggedOut') {
        let html, target = document.getElementById('ffx-preorder'), today = new Date();
        let tplData, tpl, endDate;

        // normalise locale string
        // for comparison
        this.locale = this.locale.toLowerCase();

        // require locale to be 
        // able to enter competition
        if(this.expiryDate.hasOwnProperty(this.locale)) {

            // set end date to locale 
            // specific end date
            endDate = this.expiryDate[this.locale];

            // Override html with expired
            // page if the date has been
            // passed
            if (today.getTime() > endDate.getTime()) {
                tplData = {
                    storeUrl: this.storeUrl
                }
                html = this.getExpiredHero(tplData);

            // If the campaign has not
            // expired, load the relevant
            // blade
            } else {

                switch(type) {

                    // Logged out
                    case 'loggedOut':
                        html = this.getLoggedOutHero();
                    break;


                    // Existing customers
                    case 'existing':
                        html = this.getExistingCustomerHero();
                        this.userEntered = true;
                    break;


                    // New customers
                    case 'new':
                        html = this.getNewCustomerHero();
                    break;


                    // Expired
                    case 'expired':
                        tplData = {
                            storeUrl: this.storeUrl
                        }
                        html = this.getExpiredHero(tplData);
                    break;
                }
            }
        }


        // Add content to
        // the hero
        $(target)['appendHbs'](html);


        // Hide loaders once the
        // blade has loaded
        // it's content
        this.hideLoader();


        // Show modal if
        // new user
        if (this.modalSettings === 'yes') {
            this.showModal();
        }
    }



    /* Get Expired Hero
    ============================
    When a campaign has expired,
    load in the relevant
    hero */
    getExpiredHero(data) {
        let script, template;

        // load in expired template
        script = $('#ffx-preorder-hero-template--expired').html();  
        template = Handlebars.compile(script);  

        // Return markup
        return template(data);
    }



    /* Get Logged Out Hero
    ============================
    Sets hero to display content
    relevant to logged out
    users */
    getLoggedOutHero() {
        let script = $('#ffx-preorder-hero-template--logged-out').html();  
        let template = Handlebars.compile(script);  
        let data = {
            gamertag: this.user.gamertag,
            url: this.returnUrl
        };
        
        return template(data);
    }



    /* Set Existing User Hero
    ============================
    Sets hero to display content
    relevant to logged in users
    who have already clicked
    pre-order */
    getExistingCustomerHero() {
        let script = $('#ffx-preorder-hero-template--existing').html();  
        let template = Handlebars.compile(script);  
        let data = {
            gamertag: this.user.gamertag,
            gamerpic: this.user.gamerpic,
        };
        
        return template(data);
    }



    /* Set New User Hero
    ============================
    Sets hero to display content
    relevant to logged in users
    who haven't yet been clicked
    pre-order */
    getNewCustomerHero() {
        let script = $('#ffx-preorder-hero-template--new').html();  
        let template = Handlebars.compile(script);  
        let data = {
            gamertag: this.user.gamertag,
            gamerpic: this.user.gamerpic
        };
        
        return template(data);
    }


    /* get Loader
    ===========================
    Returns handlebars html to be 
    inserted where loader is required
    */
    getLoader() {
        let script = $("#ffx-preorder-inline-loader").html();
        let template = Handlebars.compile(script);

        return template();
    }



    /* Show Loader
    ============================
    Add body class to trigger
    loading animation between
    views and on page load */
    showLoader() {
        $('body').removeClass('ffx-preorder--loaded');
    }



    /* Hide Loader
    ============================
    Remove body class to hide
    loading animation when views
    are ready */
    hideLoader() {
        $('body').addClass('ffx-preorder--loaded');
    }



    /* Show Modal
    ============================
    Add body class to trigger
    loading animation between
    the PayPal modal being
    active and inactive */
    showModal() {
        $('body').addClass('ffx-preorder-modal-active');
    }



    /* Hide Modal
    ============================
    Remove body class to trigger
    loading animation between
    the PayPal modal being
    active and inactive */
    hideModal() {
        $('body').removeClass('ffx-preorder-modal-active');
    }




    /* Register Event Handlers
    ============================
    Register global event
    handlers */
    registerEventListeners() {

        // Checkboxes
        $('#ffx-preorder__modal').on('change', '.ms-checkbox', e => {
            let label = $(e.currentTarget);
            label.find('.custom-control').toggleClass('custom-control--checked');


            // Toggle button's "disabled" property
            if (e.target.checked === true) {
                $('#order-ffx-btn').prop('disabled', false);
            } else {
                $('#order-ffx-btn').prop('disabled', true);
            }
        });


        // Log pre order click
        // user entered into competition
        if(false === true) {
            $('#ffx-preorder').on('click', '.pre-order-button', e => {
                e.preventDefault();
                
                // set entries variables
                let gamertag = this.user.gamertag;
                let locale = this.locale;

                // log pre order
                this.logPreorder(gamertag, locale);
            });            
        }

        // Overlay toggle
        $('#ffx-preorder__overlay, .ffx-preorder-modal__close').click(e => {
            e.preventDefault();
            this.hideModal();
        });


        // Escape key toggle
        $(document).keydown(e => {
            if (e.keyCode === 27) {
                e.preventDefault();
                this.hideModal();
            }
        });

    }


    /* Get Parameter by Name
    ============================
    Gets URL parameter values
    by name */
    getParameterByName(name, url = window.location.href) {

        // Format name
        name = name.replace(/[\[\]]/g, "\\$&");
        let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);


        // No results were
        // found
        if (!results) {
            return null;
        }


        // Return result
        // value
        if (!results[2]) {
            return '';
        }


        // Return values
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /* Log pre order
    ===================
    Sends Ajax post request to api
    to store users pre-order
    */
    logPreorder($gamertag, $locale) {

        let data = {
            gamertag: $gamertag,
            locale  : $locale
        };
        let loader;

        $.ajax({
            type: 'POST',
            url: this.endpoint + 'enterGamertag',
            data: data,

            // show inline loader when request
            // is sent to api
            beforeSend: e => {
                loader = this.getLoader();

                // remove entry text to prevent
                // duplicate ajax requests
                if(!this.userEntered) {
                    $('.ffx-preorder-hero__button').css({'display': 'none'})
                } else {
                    $('.ffx-preorder__hero__copy .title.existing').remove();
                }

                // replace context with loader
                $('.ffx-preorder-hero__loader__container').html(loader);
            },

            // Success
            success: e => {
				console.log(e);
                if(e.status == 'success'){
					window.location.href = this.storeUrl;
                	
                }
            },

            // Fail 
            fail: e => {
                location.reload();
            }
        })
    }

    /* Get Return URL
    ======================
    Returns current page url
    formatted correctly
     */
    getReturnUrl() {
        return 'http://www.xbox.com/' + this.locale + '/promotions/final-fantasy-XV-competition';
    }
}


// Instantiate blade
let ffxPreorder = new FfxPreorder();